using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_v2 : MonoBehaviour
{
    [SerializeField] private UI_Manager UI_Manager_Script;
    [SerializeField] private int WayPointCount;

    // Start is called before the first frame update
    void Start()
    {
        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();
        WayPointCount = GameObject.FindGameObjectsWithTag("WayPoint").Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (WayPointCount <= 0)
        {
            UI_Manager_Script.PlayerWin();
        }
    }

    public void DecWayPointCount()
    {
        WayPointCount--;
    }
}

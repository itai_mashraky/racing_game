using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour
{

    [SerializeField] private bool awakeState = false;

    private void Start()
    {
        awakeState = false;
    }

    private void Update()
    {

        if (awakeState)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, 3, 0), 0.1f);
        }
    }

    public void Activate()
    {
        awakeState = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("player dead");

        if (collision.transform.CompareTag("Player"))
        {
            //print("player dead");
        }
    }

}

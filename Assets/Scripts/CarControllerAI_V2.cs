using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CarControllerAI_V2 : CarController
{
    [SerializeField] private Transform target;
    [SerializeField] private int[] Angles;
    [SerializeField] private GameObject[] Targets;
    [SerializeField] private int Counter = 0;
    [SerializeField] private float maxDist; 

    // Start is called before the first frame update
    void Start()
    {
        Targets = GameObject.FindGameObjectsWithTag("WayPoint");

        Array.Sort(Targets, ComparByName);

        target = Targets[Counter++].transform;

       // Angles = new int[6];
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(Vector3.Distance(transform.position, target.position) < maxDist)
        {
            target = Targets[Counter++].transform;
        }
        if(Counter == Targets.Length)
        {
            Counter = 0;
        }

        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();
    }
    public override void GetInput()
    {
        Vector3 DirectionToMove = (target.position - transform.position).normalized;

        float dot = Vector3.Dot(transform.forward, DirectionToMove);

        if (dot >= 0)
        {
            verticalInput += 0.1f * Time.deltaTime;
        }
        else
        {
            if(verticalInput > 0)
            {
                verticalInput = 0;
            }
            verticalInput -= 0.1f * Time.deltaTime;
        }
        Mathf.Clamp(verticalInput, -1, 1);

        float angleToDir = Vector3.SignedAngle(transform.forward, DirectionToMove, Vector3.up);
        print(angleToDir);
        if (angleToDir <= Angles[0] && angleToDir >= Angles[1])
        {
            horizontalInput = 1f;
        }
        else if (angleToDir <= Angles[1] && angleToDir >= Angles[2])
        {
            horizontalInput = 0.7f;
        }
        else if (angleToDir <= Angles[2] && angleToDir >= Angles[3])
        {
            horizontalInput = 0.5f;
        }
        else if (angleToDir <= Angles[3] && angleToDir >= Angles[4])
        {
            horizontalInput = 0.3f;
        }
        if (angleToDir >= -Angles[5] && angleToDir <= Angles[5])
        {
            horizontalInput = 0f;
        }



    }

    public int ComparByName(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int CheckPointCounter;
    [SerializeField] private UI_Controller UI_Controller_Script;

    // Start is called before the first frame update
    void Start()
    {
        UI_Controller_Script = GameObject.Find("Canvas").GetComponent<UI_Controller>();
        CheckPointCounter = GameObject.FindGameObjectsWithTag("WayPoint").Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckPointCounter <= 0)
        {
            UI_Controller_Script.PlayerFinishedRace();
        }
    }

    public void decCheckPointCounter()
    {
        CheckPointCounter--;
    }
}

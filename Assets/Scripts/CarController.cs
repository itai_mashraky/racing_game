using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{


    protected const string HORIZONTAL = "Horizontal"; // const string for Horizontal input 
    protected const string VERTICAL = "Vertical"; // const  string for Vertical input 

    protected float horizontalInput;
    protected float verticalInput;

    protected float currentSteerAngle;
    protected float currentbreakForce;

    [SerializeField] protected bool isBreaking;

    [SerializeField] protected float motorForce;
    [SerializeField] protected float breakForce;
    [SerializeField] protected float maxSteerAngle;

    [SerializeField] protected WheelCollider frontLeftWheelCollider;
    [SerializeField] protected WheelCollider frontRightWheelCollider;
    [SerializeField] protected WheelCollider rearLeftWheelCollider;
    [SerializeField] protected WheelCollider rearRightWheelCollider;

    [SerializeField] protected Transform frontLeftWheelTransform;
    [SerializeField] protected Transform frontRightWheelTransform;
    [SerializeField] protected Transform rearLeftWheelTransform;
    [SerializeField] protected Transform rearRightWheelTransform;


    private void FixedUpdate()
    {
        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();

    }


    public virtual void GetInput()
    {
        horizontalInput = Input.GetAxis(HORIZONTAL); // input from a & d keys 
        verticalInput = Input.GetAxis(VERTICAL); // input from w & s keys
        isBreaking = Input.GetKey(KeyCode.Space); // set to true if space bar is pressed
    }

    protected void HandleMotor()
    {
        frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
        frontRightWheelCollider.motorTorque = verticalInput * motorForce;

        if (isBreaking == true)
        {
            currentbreakForce = breakForce;
        }
        else
        {
            currentbreakForce = 0f;
        }

        ApplyBreaking();
    }

    protected void ApplyBreaking()
    {
        frontRightWheelCollider.brakeTorque = currentbreakForce;
        frontLeftWheelCollider.brakeTorque = currentbreakForce;
        rearLeftWheelCollider.brakeTorque = currentbreakForce;
        rearRightWheelCollider.brakeTorque = currentbreakForce;
    }

    protected void HandleSteering()
    {
        currentSteerAngle = maxSteerAngle * horizontalInput; // ranges in 30 || -30 in value

        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    protected void UpdateWheels()
    {
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheelTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
    }

    protected void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 position;
        Quaternion rotation;

        wheelCollider.GetWorldPose(out position, out rotation);

        wheelTransform.rotation = rotation;

        wheelTransform.position = position;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform playerBody;
    private float x, y, Xrotation = 0, Sens = 100;

    // Start is called before the first frame update
    void Start()
    {
        playerBody = transform.parent.transform;
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Mouse X") * Sens * Time.deltaTime;
        y = Input.GetAxis("Mouse Y")* Sens * Time.deltaTime;

         Xrotation -= y;

        Xrotation = Mathf.Clamp(Xrotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(Xrotation, 0, 0);

        playerBody.Rotate(Vector3.up * x);

    }
}
